import smtplib, requests
from os import getcwd, getenv
from os.path import abspath, join
from time import time
from json import loads
from flask import Flask, request, jsonify, send_file, abort
from urllib.parse import unquote
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate, make_msgid
from .InvalidUsage import InvalidUsage
import uuid


from flask_cors import CORS

# environment variable > default path; abspath for better log
config_file = abspath(getenv('ANNOUNCE_CONFIG', 'instance/config.py'))

app = Flask(__name__)
CORS(app)

app.config.from_pyfile(config_file)


@app.route('/')
def entry_point():
    raise InvalidUsage('Wrong ressource', 400)


@app.route('/mailer', methods=['POST'])
def handle_request():
    html = unquote(request.form.get('html', ''))
    text = unquote(request.form.get('text', ''))
    subject = unquote(request.form.get('sub', ''))
    token = unquote(request.form.get('token', ''))

    if check_auth(token) == False:
        raise InvalidUsage('Unauthorized', 401)

    if html != '' and text != '':
        if subject != '':
            before = time()
            send_mail(html, text, subject)
            took = time() - before
        else:
            raise InvalidUsage('Subject must not be empty!', 400)
    else:
        raise InvalidUsage('Message (html, txt) must not be empty!', 400)

    return '{"_status":"OK", "_code":200, "took":' + str(took) + ', "message":"E-Mail successfully sent."}'

@app.route('/upload', methods=['POST'])
def handle_upload():
    token = unquote(request.form.get('token', ''))

    if check_auth(token) == False:
        raise InvalidUsage('Unauthorized', 401)

    try:
        f = request.files['file']
        fn = str(uuid.uuid4()) + '.' + f.filename.split('.')[-1]
        f.save(join('/data/', fn))
        return '{"filename": "' + fn + '"}', 200
    except Exception as e:
        raise InvalidUsage('Error saving file', 400)

@app.route('/upload/<filename>', methods=['GET'])
def handle_upload_get(filename=None):
    try:
        return send_file('/data/' + filename)
    except FileNotFoundError:
        raise InvalidUsage('File not found', 400)

def send_mail(html, text, subj):
    msg = MIMEMultipart('alternative')

    msg['Subject'] = subj
    msg['From'] = app.config['MAIL_SENDER']
    msg['To'] = app.config['MAIL_RECEIPIENT']
    msg["Date"] = formatdate(localtime=True)
    msg['Message-ID'] = make_msgid()

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    msg.attach(part1)
    msg.attach(part2)

    try:
        smtp = smtplib.SMTP(app.config['SMTP_HOST'], app.config['SMTP_PORT'])
        smtp.starttls()
        smtp.login(app.config['SMTP_USER'], app.config['SMTP_PASSWORD'])
        smtp.send_message(msg)
    except:
        raise InvalidUsage('SMTP host or credentials misconfigured.', 500)

    smtp.quit()


def check_auth(token):
    group_met = False

    try:
        user_id_api = requests.get(app.config['API_DOMAIN'] + '/sessions?where={"token":"%s"}' % token, headers={'Authorization': token})
        user_id = loads(user_id_api.text)['_items'][0]['user']
        
        print(user_id, flush=True)

        api = requests.get(app.config['API_DOMAIN'] + '/groupmemberships?embedded={"group":1}&where={"user":"%s"}' % user_id, headers={'Authorization': token})
    except:
        raise InvalidUsage('AMIV-API address misconfigured or unreachable', 500)
    try:
        string = api.text
        obj = loads(string)

        content = obj['_items']

        for i in range(0, obj['_meta']['total']):
            if str(content[i]['group']['name']) == app.config['REQUIRED_GROUP']:
                group_met = True
                break

    except KeyError:
        print(obj, flush=True)
        if str(obj["_status"]) == "ERR":
            if obj["_error"]["code"] == 401:
                raise InvalidUsage('Invalid or expired token.', 401)
        else:
            raise InvalidUsage('AMIV-API returned unknown response.', 500)
    except:
        raise InvalidUsage('AMIV-API returned unknown response. Or Group membership could not be checked.', 500)

    return group_met


@app.errorhandler(InvalidUsage)
def handle_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/englishman')
def teapot():
    raise InvalidUsage('I\'m a teapot', 418)
